# Sailor

**Sample website build with html5, Sass, Breakpoint, flexBox and js**

Sailor uses Breakpoint for media queries in Sass and it is super simple. 
FlexBox is used for simplifying it even more!

## How to use it?
**Here comes gulp**. Clone or download the project on your computer and use your terminal to run the project. First go to **downloaded/cloned** folder and use command `npm install`. Next go to **app** folder and run the command `bower install` - this will install all the components listed as dependencies in your bower.json file. Finally run command `gulp`. This will get you running the project! 

In **gulpfile.js** you will find all the tasks that are easing your life - browser-sync, plumber, uglify and more. 
**Config.rb** defines all dependencies between files and their assets.