$(window).scroll(function() {
 //  if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {
 //             window.scrollTo (100)
 // }else{
      if($(this).scrollTop() > 100) { /*height in pixels when the navbar background becomes transparent*/
          $('.navigation__icon').addClass('navigation__icon--color');
      } else {
          $('.navigation__icon').removeClass('navigation__icon--color');
      }
  //  }
});

// Cache selectors
var lastId,
    menu = $("#menu"),
    menuHeight = menu.outerHeight()+40,
    menuHeightDesktop = menu.outerHeight(),
    // All list items
    menuItems = menu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  if($(window).width() <= 768){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-menuHeight+1;
  $('html, body').stop().animate({
      scrollTop: offsetTop
  }, 1000);
  e.preventDefault();
}
});

menuItems.click(function(e){
  if($(window).width() >= 768){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-menuHeightDesktop+1;
  $('html, body').stop().animate({
      scrollTop: offsetTop
  }, 1000);
  e.preventDefault();
}
});

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+menuHeight;

   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";

   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("wrapper__link--active")
         .end().filter("[href='#"+id+"']").parent().addClass("wrapper__link--active");
   }
});


///// drop down menu and menu icon animated  ///////////////////

$(document).ready(function(){

  if($(window).width() <= 768){
    $(".navigation__icon").on("click",function(e) {
        e.preventDefault();
        $(".wrapper__list").slideToggle("reveal");
        $("#icon-element-1").toggleClass("act");
        $("#icon-element-2").toggleClass("act2");
        $("#icon-element-3").toggleClass("act3");
    });

    $(".wrapper__list").on("click",function(e) {
        $(".wrapper__list").slideUp("reveal");
        $("#icon-element-1").toggleClass("act");
        $("#icon-element-2").toggleClass("act2");
        $("#icon-element-3").toggleClass("act3");
    });
    }
});



///// default contronls disabled ///////////////////
   var video = document.getElementById("video");
   video.controls = false;
///// looped the video ///////////////////
   $('video').attr('loop','loop');
///// toggle playpause ///////////////////
 $(document).ready(function() {
     var playing = false;

     $('#playpause').click(function() {
         $(this).toggleClass("playpause");
         $(this).toggleClass("expanded");

         if (playing == false) {
             document.getElementById('video').play();
             playing = true;
             playpause.title = "pause video";
         } else {
             document.getElementById('video').pause();
             playing = false;
             playpause.title = "continue watching";
           }

     });

 });


///// scrolly button  ///////////////////
$("#button__scrolly").click(function() {
    $('html, body').animate({
        scrollTop: $("#about").offset().top
    }, 1000, 'swing');
});


///// footer - email input  ///////////////////
$(".footer__input").focus(function(){
  $(this).parent().addClass("is-active is-completed");
});

$(".footer__input").focusout(function(){
  if($(this).val() === "")
    $(this).parent().removeClass("is-completed");
  $(this).parent().removeClass("is-active");
});
