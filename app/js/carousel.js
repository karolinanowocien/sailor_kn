var $carousel = $('.carousel');
var $seats = $('.carousel-seat');


 //on swipe-right/////
 $('.toggle').on('swiperight', function(e)  {
   var $newSeat;
   var $el = $('.is-ref');
   var $active =$('.active');
   var $currSliderControl = $(e.currentTarget);
  // Info: e.target is what triggers the event dispatcher to trigger and e.currentTarget is what you assigned your listener to.


  $el.removeClass('is-ref');
  $carousel.removeClass('is-reversing--left');
  if ($currSliderControl.data('toggle') === 'previous') {
    $newSeat = next($el);
    $carousel.removeClass('is-reversing--right');
  } else {
    $newSeat = prev($el);
    $carousel.addClass('is-reversing--right');
  }
  /////////////

  $newSeat.addClass('is-ref').css('order', 2);
  for (var i = 2; i <= $seats.length; i++) {
    $newSeat = next($newSeat).css('order', i);
  }

  $active.removeClass('active');
  $carousel.removeClass('is-reversing--left');
  if ($currSliderControl.data('toggle') === 'next') {
    $newSeat = next($active);
    $carousel.removeClass('is-reversing--right');
  } else {
    $newSeat = prev($active);
    $carousel.addClass('is-reversing--right');
  }


  $newSeat.addClass('active').css('order', 3);
  for (var i = -2; i <= $seats.length; i++) {
    $newSeat = next($newSeat).css('order', i);
  }


  $carousel.removeClass('is-set');
  return setTimeout(function() {
    return $carousel.addClass('is-set');
  }, 50);

  function next($el) {
    if ($el.next().length) {
      return $el.next();
    } else {
      return $seats.first();
    }
  }

  function prev($el) {
    if ($el.prev().length) {
      return $el.prev();
    } else {
      return $seats.last();
    }
  }
});

//on swipe-left/////
$('.toggle').on('swipeleft', function(e) {
  var $newSeat;
  var $el = $('.is-ref');
  var $active =$('.active');
  var $currSliderControl = $(e.currentTarget);


 $el.removeClass('is-ref');
 $carousel.removeClass('is-reversing--right');
 if ($currSliderControl.data('toggle') === 'next') {
   $newSeat = prev($el);
   $carousel.removeClass('is-reversing--left');
 } else {
   $newSeat = next($el);
   $carousel.addClass('is-reversing--left');
 }
 /////////////

 $newSeat.addClass('is-ref').css('order', 3);
 for (var i = 2; i <= $seats.length; i++) {
   $newSeat = prev($newSeat).css('order', i);
 }

 $active.removeClass('active');
 $carousel.removeClass('is-reversing--right');
 if ($currSliderControl.data('toggle') === 'previous') {
   $newSeat = prev($active);
   $carousel.removeClass('is-reversing--left');
 } else {
   $newSeat = next($active);
   $carousel.addClass('is-reversing--left');
 }


 $newSeat.addClass('active').css('order', 3);
 for (var i = -2; i <= $seats.length; i++) {
   $newSeat = next($newSeat).css('order', i);
 }


 $carousel.removeClass('is-set');
 return setTimeout(function() {
   return $carousel.addClass('is-set');
 }, 50);

 function prev($el) {
   if ($el.prev().length) {
     return $el.prev();
   } else {
     return $seats.last();
   }
 }

 function next($el) {
   if ($el.next().length) {
     return $el.next();
   } else {
     return $seats.first();
   }
 }
});


////////////////////////////////////////////////////////////////////////////
//on click/////
$('.toggle').on('click', function(e)  {
  var $newSeat;
  var $el = $('.is-ref');
  var $active =$('.active');
  var $currSliderControl = $(e.currentTarget);
 // Info: e.target is what triggers the event dispatcher to trigger and e.currentTarget is what you assigned your listener to.


 $el.removeClass('is-ref');
 $carousel.removeClass('is-reversing--left');
 if ($currSliderControl.data('toggle') === 'previous') {
   $newSeat = next($el);
   $carousel.removeClass('is-reversing--right');
 } else {
   $newSeat = prev($el);
   $carousel.addClass('is-reversing--right');
 }
 /////////////

 $newSeat.addClass('is-ref').css('order', 2);
 for (var i = 2; i <= $seats.length; i++) {
   $newSeat = next($newSeat).css('order', i);
 }

 $active.removeClass('active');
 $carousel.removeClass('is-reversing--left');
 if ($currSliderControl.data('toggle') === 'next') {
   $newSeat = next($active);
   $carousel.removeClass('is-reversing--right');
 } else {
   $newSeat = prev($active);
   $carousel.addClass('is-reversing--right');
 }


 $newSeat.addClass('active').css('order', 3);
 for (var i = -2; i <= $seats.length; i++) {
   $newSeat = next($newSeat).css('order', i);
 }


 $carousel.removeClass('is-set');
 return setTimeout(function() {
   return $carousel.addClass('is-set');
 }, 50);

 function next($el) {
   if ($el.next().length) {
     return $el.next();
   } else {
     return $seats.first();
   }
 }

 function prev($el) {
   if ($el.prev().length) {
     return $el.prev();
   } else {
     return $seats.last();
   }
 }
});
